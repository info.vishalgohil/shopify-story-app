window._ = require('lodash');

window.$$$ = require('jquery');

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let Zuck = require('zuck.js');

const host = process.env.MIX_APP_URL;
const apiEndPoint = `${host}/api/shop`;
const shopifyDomain = Shopify.shop;

let shopifyID = null;

let products = [];

let zuke = null;
function initZuke(stories) {

    zuke = new Zuck('stories', {
        avatars: false,
        stories: stories,
        callbacks:  {
            onView (storyId) {

                axios.post(`${apiEndPoint}/${shopifyID}/products/${storyId}/view`);
            },
        },
    });

    if (zuke) {

        $$$(document).on('click', '#zuck-modal-content .story-viewer .tip.link', (e) => {

            e.preventDefault();

            let storyId = $$$(e.target).parent().parent().parent().data('story-id');
            let href = $$$(e.target).attr('href');
            axios.post(`${apiEndPoint}/${shopifyID}/products/${storyId}/visit`).then(() => {
                window.location = href;
            });
        });

        axios.post(`${apiEndPoint}/${shopifyID}/load`);
    }
}

function loadStories() {

    axios.get(`${apiEndPoint}/${shopifyID}/products`).then(response => {

        products = response.data.products;

        let stories = [];
        products.forEach(product => {

            if (product.image) {

                let storyItems = [];
                product.images.forEach(image => {

                    storyItems.push({
                        id: image.id,
                        type: 'photo',
                        length: 3,
                        src: image.src,
                        link: `https://${shopifyDomain}/products/${product.handle}`,
                        linkText: 'Visit'
                    });
                });

                stories.push({
                    id: product.id,
                    photo: product.image,
                    name: product.title,
                    link: `https://${shopifyDomain}/products/${product.handle}`,
                    items: storyItems
                });
            }
        });

        if (stories.length) {

            initZuke(stories);
        }
    });
}

(function init() {

    axios.get(`${apiEndPoint}?shopify_domain=${shopifyDomain}`).then(response => {

        shopifyID = response.data.shop.shopify_id;

        let head = document.head;
        let link = document.createElement("link");
        link.type = "text/css";
        link.rel = "stylesheet";
        link.href = `${host}/css/product-gallery.css`;
        head.appendChild(link);

        if ($$$('body').hasClass('template-index') && !$$$('#stories').length) {

            $$$('body').prepend(`<div id="stories" class="storiesWrapper"></div>`);
        }

        loadStories();
    }).catch((error) => {
        console.log(error.response);
    });
})();
