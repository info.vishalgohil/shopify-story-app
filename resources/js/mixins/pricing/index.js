const pricingMixin = {
    methods: {
        _initPricing() {

            if (this.currentTemplate !== 'pricing.index') {
                return;
            }

            $(document).ready(function(){
                $('.pro').click(function(){
                    $('.pro').removeClass(".pro active");
                    $(this).addClass("active");
                });
            });
        },
    }
};

export default pricingMixin;
