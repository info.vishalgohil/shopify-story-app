const helperMixin = {
    methods: {
        toMoneyFormat(value) {
            return this.shop.money_format.replace('{{amount}}', value);
        },
        updateTitleBar() {

            let titleBarOptions = {};

            switch (this.currentTemplate) {
                case 'dashboard.index':
                    titleBarOptions.title = 'Dashboard';
                    break;
                case 'settings.index':
                    titleBarOptions.title = 'Appearance';
                    break;
                case 'pricing.index':
                    titleBarOptions.title = 'Plans & Pricing';
                    break;
            }

            if (Object.entries(titleBarOptions).length !== 0) {
                this.titleBar.set(titleBarOptions);
            }
        },
        dispatchToast(message, isError = false) {

            let toastOptions = {
                message: message,
                duration: 3000,
                isError: isError
            };

            let toast = actions.Toast;
            let toastMessage = toast.create(embeddedApp, toastOptions);
            toastMessage.dispatch(toast.Action.SHOW);
        },
        getShopDetails() {

            axios.get(`${this.apiEndPoint}`).then(response => {

                this.shop = response.data.shop;
            });
        },
        getCurrentTemplate() {

            this.currentTemplate = $(`[data-page-template]`).data('page-template');
        }
    },
    watch: {
        currentTemplate() {
            // this.updateTitleBar();
        }
    }
};

export default helperMixin;
