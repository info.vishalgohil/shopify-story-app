let Button = actions.Button;
let Modal = actions.Modal;
let ResourcePicker = actions.ResourcePicker;

const productIDPrefix = 'gid://shopify/Product/';

const settingsMixin = {
    data() {
        return {
            products: [],
            widget: {
                title: '',
                story_style: 1,
                /*style: {
                    arrow_color: '',
                    product_title_color: '',
                    product_price_color: '',
                    product_button_color: '',
                    product_button_background_color: '',
                    product_button_text: '',
                    product_column_per_row: '',
                    product_image_style: '',
                }*/
            }
        }
    },
    methods: {
        _initSettings() {

            if (this.currentTemplate !== 'settings.index') {
                return;
            }

            this.updateProductPicker();
            this.getWidget();
            this.getProducts();

            this.initOwlCarousel();
        },
        getWidget() {

            axios.get(`${this.apiEndPoint}/widget`).then(response => {
                this.widget = response.data;
            }).catch(error => {
                console.log(error.response);
                this.dispatchToast(error.response.data.error ? error.response.data.error : error.response.data.message, true);
            });
        },
        getProducts() {

            axios.get(`${this.apiEndPoint}/products`).then(response => {
                this.products = response.data.products;
            }).catch(error => {
                console.log(error.response);
                this.dispatchToast(error.response.data.error ? error.response.data.error : error.response.data.message, true);
            });
        },
        storeProducts(productIDs) {

            axios.post(`${this.apiEndPoint}/products`, {ids: productIDs}).then(response => {
                this.getProducts();
                this.dispatchToast(response.data.message);
            }).catch(error => {
                console.log(error.response);
                this.dispatchToast(error.response.data.error ? error.response.data.error : error.response.data.message, true);
            });
        },
        removeProduct(productID) {

            this.confirmationModal.set({
                title: 'Remove Product?',
                message: 'Are you sure to remove this product?'
            });

            this.confirmationModalPrimaryButton.set({label: 'Remove'});
            this.confirmationModalPrimaryButton.subscribe(Button.Action.CLICK, () => {

                axios.delete(`${this.apiEndPoint}/products/${productID}`).then(response => {

                    this.dispatchToast(response.data.message);
                    this.getProducts();
                }).catch(error => {
                    console.log(error.response);
                    this.dispatchToast(error.response.data.error ? error.response.data.error : error.response.data.message, true);
                }).then(() => {
                    this.confirmationModal.dispatch(Modal.Action.CLOSE);
                });
            });

            this.confirmationModal.dispatch(Modal.Action.OPEN);
        },
        dispatchProductPicker() {

            if (this.products.length < 10) {
                this.productPicker.dispatch(ResourcePicker.Action.OPEN);
            }
        },
        updateProductPicker() {

            this.productPicker.set({showHidden: false, showVariants: false});

            this.productPicker.subscribe(ResourcePicker.Action.SELECT, ({selection}) => {

                let selectedProductIDs = [];
                let selectedExistedProducts = [];
                selection.forEach((selectedProduct) => {

                    let productID = selectedProduct.id.replace(productIDPrefix, '');
                    let selectedExistedProduct = this.products.find(product => product.id == productID);
                    if (selectedExistedProduct) {
                        selectedExistedProducts.push(selectedExistedProduct);
                    } else {
                        selectedProductIDs.push(selectedProduct.id);
                    }
                });

                if (selectedExistedProducts.length) {

                    let msg = `${selectedExistedProducts.length} product(s) already exists!`;
                    this.dispatchToast(msg, true);
                } else {

                    this.storeProducts(selectedProductIDs);
                }
            });
        },
        initOwlCarousel() {
            // $('.owl2').owlCarousel({
            //     loop: true,
            //     margin: 10,
            //     nav: true,
            //     dots: false,
            //     autoplay: true,
            //     responsive: {
            //         0: {
            //             items: 1
            //         },
            //         600: {
            //             items: 1
            //         },
            //         1000: {
            //             items: 3
            //         }
            //     }
            // });
        }
    }
};

export default settingsMixin;
