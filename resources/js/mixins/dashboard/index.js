let Chart = require('chart.js');

const dashboardMixin = {
    data() {
        return {
            totalViews: 0,
            totalClicks: 0,
            topProducts: [],
        }
    },
    methods: {
        _initDashboard() {

            if (this.currentTemplate !== 'dashboard.index') {
                return;
            }

            axios.get(`${this.apiEndPoint}/dashboard`).then(response => {

                this.totalViews = response.data.total_views;
                this.totalClicks = response.data.total_clicks;
                this.topProducts = response.data.top_products;

                this.initGraph();
            });
        },
        initGraph() {

            let yLabels = {
                0: '$0',
                2: '$5',
                4: '$10',
                6: '$20',
                8: '$40',
                10: '$80',
            };
            let ctx = document.getElementById('sales-chart');

            let myLineChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP'],
                    datasets: [{
                        label: '',
                        data: [0, 2, 3, 2, 8, 4, 8, 7, 9, 9, 5, 12],
                        backgroundColor: [
                            'rgba(209, 110, 254, 0)',
                            'rgba(209, 110, 254, 1)',
                            'rgba(209, 110, 254, 1)',
                            'rgba(209, 110, 254, 1)',
                            'rgba(209, 110, 254, 1)',
                            'rgba(209, 110, 254, 1)',
                            'rgba(209, 110, 254, 1)',
                            'rgba(209, 110, 254, 1)',
                            'rgba(209, 110, 254, 1)',
                            'rgba(209, 110, 254, 1)',
                            'rgba(209, 110, 254, 1)',
                        ],
                        borderColor: [
                            'rgba(209, 110, 254, 1)',
                            'rgba(209, 110, 254, 1)',
                            'rgba(209, 110, 254, 1)',
                            'rgba(209, 110, 254, 1)',
                            'rgba(209, 110, 254, 1)',
                            'rgba(209, 110, 254, 1)'
                        ],
                        borderWidth: 3
                    }]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: ''
                    },
                    legend: {
                        display: false
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            barPercentage: 1,
                            categoryPercentage: 0.5,
                            gridLines: {
                                display: true
                            },
                            ticks: {
                                fontColor: "#8f9092"
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                callback: function (value, index, values) {
                                    return yLabels[value];
                                }
                            }
                        }]
                    }
                }
            });
        }
    }
};

export default dashboardMixin;
