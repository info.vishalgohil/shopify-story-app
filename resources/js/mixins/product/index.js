let Button = actions.Button;
let Modal = actions.Modal;
let ResourcePicker = actions.ResourcePicker;

const productIDPrefix = 'gid://shopify/Product/';

const productsMixin = {
    data() {
        return {
            products: [],
            isProductsLoading: false
        }
    },
    methods: {
        _initProducts() {

            if (this.currentTemplate !== 'product.index') {
                return;
            }

            this.updateProductPicker();
            this.getProducts();
        },
        getProducts() {

            axios.get(`${this.apiEndPoint}/products`).then(response => {
                this.products = response.data.products;
            }).catch(error => {
                console.log(error.response);
                this.dispatchToast(error.response.data.error ? error.response.data.error : error.response.data.message, true);
            });
        },
        storeProducts(products) {

            axios.post(`${this.apiEndPoint}/products`, {products: products}).then(response => {
                this.getProducts();
                this.dispatchToast(response.data.message);
            }).catch(error => {
                console.log(error.response);
                this.dispatchToast(error.response.data.error ? error.response.data.error : error.response.data.message, true);
            });
        },
        removeProduct(productID) {

            this.confirmationModal.set({
                title: 'Remove Product?',
                message: 'Are you sure to remove this product?'
            });

            this.confirmationModalPrimaryButton.set({label: 'Remove'});
            this.confirmationModalPrimaryButton.subscribe(Button.Action.CLICK, () => {

                axios.delete(`${this.apiEndPoint}/products/${productID}`).then(response => {

                    this.dispatchToast(response.data.message);
                    this.getProducts();
                }).catch(error => {
                    console.log(error.response);
                    this.dispatchToast(error.response.data.error ? error.response.data.error : error.response.data.message, true);
                }).then(() => {
                    this.confirmationModal.dispatch(Modal.Action.CLOSE);
                });
            });

            this.confirmationModal.dispatch(Modal.Action.OPEN);
        },
        dispatchProductPicker() {

            this.productPicker.dispatch(ResourcePicker.Action.OPEN);
        },
        updateProductPicker() {

            this.productPicker.set({showHidden: false, showVariants: false});

            this.productPicker.subscribe(ResourcePicker.Action.SELECT, ({selection}) => {

                let selectedProducts = [];
                let selectedExistedProducts = [];
                selection.forEach((selectedProduct) => {

                    let productID = selectedProduct.id.replace(productIDPrefix, '');
                    let selectedExistedProduct = this.products.find(product => product.id == productID);
                    if (selectedExistedProduct) {
                        selectedExistedProducts.push(selectedExistedProduct);
                    } else {
                        selectedProducts.push({
                            id: productID,
                            title: selectedProduct.title,
                            handle: selectedProduct.handle,
                            images: selectedProduct.images,
                        });
                    }
                });

                if (selectedExistedProducts.length) {

                    let msg = `${selectedExistedProducts.length} product(s) already exists!`;
                    this.dispatchToast(msg, true);
                } else {

                    this.storeProducts(selectedProducts);
                }
            });
        }
    }
};

export default productsMixin;
