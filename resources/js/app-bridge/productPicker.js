let ResourcePicker = actions.ResourcePicker;

let productPicker = ResourcePicker.create(embeddedApp, {
    resourceType: ResourcePicker.ResourceType.Product,
});

export default productPicker;
