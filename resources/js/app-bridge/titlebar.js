let TitleBar = actions.TitleBar;
let Button = actions.Button;
let Redirect = actions.Redirect;

let dashboardButton = Button.create(embeddedApp, {label: 'Dashboard'});
dashboardButton.subscribe(Button.Action.CLICK, () => {
    embeddedApp.dispatch(Redirect.toApp({path: '/'}));
});

let appearanceButton = Button.create(embeddedApp, {label: 'Appearance'});
appearanceButton.subscribe(Button.Action.CLICK, () => {
    embeddedApp.dispatch(Redirect.toApp({path: '/settings'}));
});

let pricingButton = Button.create(embeddedApp, {label: 'Plans & Pricing'});
pricingButton.subscribe(Button.Action.CLICK, () => {
    embeddedApp.dispatch(Redirect.toApp({path: '/pricing'}));
});

let titleBar = TitleBar.create(embeddedApp, {
    // buttons: {
    //     secondary: [
    //         dashboardButton,
    //         appearanceButton,
    //         pricingButton
    //     ],
    // },
});

export default titleBar;
