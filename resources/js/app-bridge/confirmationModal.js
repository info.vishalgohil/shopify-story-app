let Button = actions.Button;
let Modal = actions.Modal;

let confirmationModalPrimaryButton = Button.create(embeddedApp, {label: '', style: Button.Style.Danger});
let confirmationModal = Modal.create(embeddedApp, {
    title: '',
    message: '',
    footer: {
        buttons: {
            primary: confirmationModalPrimaryButton
        },
    }
});

export {confirmationModalPrimaryButton, confirmationModal};
