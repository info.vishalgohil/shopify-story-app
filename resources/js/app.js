window._ = require('lodash');

window.$ = require('jquery');

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    // console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

window.Vue = require('vue');

import titleBar from './app-bridge/titlebar';
import {confirmationModalPrimaryButton, confirmationModal} from './app-bridge/confirmationModal';
import productPicker from './app-bridge/productPicker';
import helperMixin from './mixins/helper';
import dashboardMixin from './mixins/dashboard/index';
import settingsMixin from './mixins/settings/index';
import pricingMixin from './mixins/pricing/index';


const host = process.env.MIX_APP_URL;
const apiEndPoint = `${host}/api`;

const vueApp = new Vue({
    el: '#pgApp',
    mixins: [
        helperMixin,
        dashboardMixin,
        settingsMixin,
        pricingMixin,
    ],
    data: {
        host: host,
        apiEndPoint: apiEndPoint,
        titleBar: titleBar,
        confirmationModalPrimaryButton: confirmationModalPrimaryButton,
        confirmationModal: confirmationModal,
        productPicker: productPicker,
        currentTemplate: null,
        shop: {},
    },
    methods: {
        initApp() {

            this.getCurrentTemplate();
            this.getShopDetails();
            this._initDashboard();
            this._initSettings();
            this._initPricing();
        }
    },
    mounted() {
        this.initApp();
    }
});
