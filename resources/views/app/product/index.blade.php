@extends('app.layouts.app')

@section('content')
    <div data-page-template="product.index">
        <div class="Polaris-Page Polaris-Page--fullWidth">
            <div class="Polaris-Page__Content">
                <div class="Polaris-Card">
                    <div class="Polaris-Card__Header">
                        <h2 class="Polaris-Heading">Products
                            <button type="button" class="Polaris-Button Polaris-Button--primary" v-bind:class="{'Polaris-Button--disabled': products.length >= 10}" v-bind:disabled="products.length >= 10" v-on:click="dispatchProductPicker()" style="float: right"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Add Product</span></span></button>
                        </h2>
                        (Showing @{{products.length}} of 10 products)<br>
                    </div>
                    <div class="Polaris-Card__Section">
                        <div class="Polaris-Card mt-1">
                            <div class="table-responsive">
                                <table>
                                    <tr>
                                        <th class="text-left">Product Image</th>
                                        <th class="text-left">Title</th>
                                        <th class="text-center">Action</th>
                                        <th class="text-center">Visits</th>
                                    </tr>
                                    <tr v-for="product in products">
                                        <td class="text-left"><img v-bind:src="product.image" height="50px" width="50px"></td>
                                        <td class="text-left">@{{product.title}}</td>
                                        <td class="text-center"><button type="button" class="Polaris-Button Polaris-Button--primary" v-on:click="removeProduct(product.id)"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Remove</span></span></button></td>
                                        <td class="text-center">@{{product.total_visits}}</td>
                                    </tr>
                                    <tr v-if="!products.length">
                                        <td class="text-center" colspan="3">@{{ (isProductsLoading) ? 'Loading...' : 'No data available' }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
