@extends('app.layouts.app')

@section('content')

<div data-page-template="dashboard.index">

    <div class="tab-content py-5 px-3 px-sm-0" id="nav-tabContent">
        <section class="dashboard">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p767">
                        <div class="dash-card">
                            <p class="card-upper">
                                <span class="card-conte">Total Views</span>
                                <span class="right-text card-i"><i class="far fa-eye"></i></span>
                            </p>
                            <p class="card-price">@{{totalViews}}</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p767">
                        <div class="dash-card">
                            <p class="card-upper">
                                <span class="card-conte">Total Clicks</span>
                                <span class="right-text card-i"><i class="fab fa-telegram-plane"></i></span>
                            </p>
                            <p class="card-price">@{{totalClicks}}</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p767">
                        <div class="dash-card">
                            <p class="card-upper">
                                <span class="card-conte">Total Sales</span>
                                <span class="right-text card-i"><i class="fas fa-chart-bar"></i></span>
                            </p>
                            <p class="card-price">$0</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p767">
                        <div class="dash-card">
                            <p class="card-upper">
                                <span class="card-conte">Conversation Rate</span>
                                <span class="right-text card-i"><i class="fas fa-arrow-circle-up"></i></span>
                            </p>
                            <p class="card-price">0%</p>
                        </div>
                    </div>
                </div>
                <div class="row ptgap3">
                    <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                        <div class=" dashboard-maintables">
                            <h4 class="pgap2 secondcolor nova_bold newc-top">Top 5 Products</h4>
                            <div v-for="(topProduct, index) in topProducts" class="row border-bttm">
                                <div class="col-lg-6 col-md-8 col-sm-8 col-6 align-self-center">
                                    <div class="dash-volvo">
                                        <div class="dash-img">
                                            <img v-bind:src="(topProduct.image ? topProduct.image : '/images/no-image.png')" class="img-fluid">
                                        </div>
                                        <div class="dash-cont">
                                            <p class="center-text dash-pasta">@{{topProduct.title}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-2 col-sm-2 col-3 align-self-center">
                                    <p class="center-text clicke">Clicks <span class="table-pricee nova_bold">@{{topProduct.visits}}</span></p>
                                </div>
                                <div class="col-lg-3 col-md-2 col-sm-2 col-3 align-self-center">
                                    <p class="center-text clicke">Sales <span class="table-pricee nova_bold">$0</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-12 col-sm-12 col-12">
                        <section class="dashboard-maintables dashboard-maintables-now">
                            <h4 class="pgap2 secondcolor nova_bold newc-top">Sales Overview</h4>
                            <canvas id="sales-chart" width="400" height="240"></canvas>
                        </section>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection
