<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('shopify-app.app_name') }}</title>

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" />
        @yield('styles')
    </head>

    <body>
        <div id="pgApp">
            <section id="tabs" class="pgap1 main-container">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">

                            <nav class="navbar navbar-expand-lg bwheat navbar-dark">
                                <div class="container-fluid">
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                                        <span><i class="fas fa-bars"></i></span>
                                    </button>
                                    <div class="collapse navbar-collapse" id="collapsibleNavbar">
                                        <ul class="navbar-nav mr-auto blow-767">
                                            <li class="nav-item">
                                                <a class="nav-link secondcolor {{ (strpos(Route::currentRouteName(), 'home') === 0) ? 'active' : '' }}" href="{{route('home')}}">Dashboard</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link secondcolor {{ (strpos(Route::currentRouteName(), 'settings') === 0) ? 'active' : '' }}" href="{{route('settings')}}">Appearance</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link secondcolor {{ (strpos(Route::currentRouteName(), 'pricing') === 0) ? 'active' : '' }}" href="{{route('pricing')}}">Plans & Pricing</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>

                            @yield('content')

                        </div>
                    </div>
                </div>
            </section>
        </div>

        <script src="https://unpkg.com/@shopify/app-bridge{{ config('shopify-app.appbridge_version') ? '@'.config('shopify-app.appbridge_version') : '' }}"></script>
        <script>
            const AppBridge = window['app-bridge'];
            const createApp = AppBridge.default;
            const embeddedApp = createApp({
                apiKey: '{{ config('shopify-app.api_key') }}',
                shopOrigin: '{{ ShopifyApp::shop()->shopify_domain }}',
                forceRedirect: true,
            });
            const actions = AppBridge.actions;
        </script>

        @include('shopify-app::partials.flash_messages')

        <script src="{{ mix('/js/app.js') }}"></script>
        @yield('scripts')
    </body>
</html>
