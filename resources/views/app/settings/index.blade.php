@extends('app.layouts.app')

@section('content')

    <div data-page-template="settings.index">
        <div class="tab-content py-4 px-3 px-sm-0" id="nav-tabContent">
            <section class="appear">
                <div class="container-fluid">
                    <h1 class="">Appearance</h1>
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class=" dashboard-maintables appearance-maintables newc-table">
                                <h3 class="pgap2 secondcolor nova_bold border-bottom">Settings</h3>
                                <div class="input-form border-bottom-dark pgap3">
                                    <p>
                                        <label>Title</label>
                                        <input type="text" name="title" placeholder="Your Title">
                                    </p>
                                    <p>
                                        <label>Select Story Style</label>
                                        <select class="form-control">
                                            <option>Style 01</option>
                                        </select>
                                    </p>
                                </div>
                                <h2 class="appear-table-head ptgap2">Select Products</h2>
                                <div class="row border-bttm2" v-for="product in products" v-bind:key="product.id">
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-9 main-claasss align-self-center">
                                        <div class="main-volvo">
                                            <div class="volvo-img">
                                                <img v-bind:src="(product.image ? product.image : '/images/no-image.png')" class="img-fluid">
                                            </div>
                                            <div class="volvo-cont">
                                                <p class="center-text clicke-pro">@{{product.title}}</p>
                                                <p class="table-pricee-pro" v-html="toMoneyFormat(product.price)"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-3 inner-claasss align-self-center">
                                        <p class="center-text clickey clicke">
                                            <a v-on:click="removeProduct(product.id)" class="trigger-btn" data-toggle="modal">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="pro-bttn pgap3 appear-bttn">
                                    <a class="price-content-button" v-if="products.length < 10" v-on:click="dispatchProductPicker()">Choose Products</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                            <section class="dashboard-maintables appearance-maintables">
                                <h3 class="pgap2 secondcolor nova_bold border-bottom">Preview</h3>
                                <h3 class="pgap2 black ptgap4 pgap3 nova_bold center-text nrew-arrivaal"></h3>
                                <div class="owl-carousel owl2 owl-theme">
                                    <div class="item" v-for="product in products" v-bind:key="product.id" v-if="product.image">
                                        <div class="card-pro card-carousel-pro">
                                            <div class="card card-carousel">
                                                <div class="img-card2">
                                                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                                                        <div class="mainflip">
                                                            <div class="frontside">
                                                                <div class="card">
                                                                    <div class="card-body text-center">
                                                                        <p class="pbgap3"><img v-bind:src="(product.image ? product.image : '/images/no-image.png')" alt="card image"></p>
                                                                        <h4 class="card-title black">@{{product.title}}</h4>
                                                                        <p class="card-text primecolor nova_bold" v-html="toMoneyFormat(product.price)"></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

@endsection
