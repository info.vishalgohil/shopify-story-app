@extends('app.layouts.app')

@section('content')

<div data-page-template="pricing.index">
    <div class="tab-content py-5 px-3 px-sm-0" id="nav-tabContent">
        <section class="main-content-second" id="vtwo">
            <div class="container-fluid">
                <h1 class="morepan">Plans & Pricing</h1>
                <h4 class="pbgap2 morepan2">Choose a plan that's right for your business</h4>
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="price-content-second pro newc-price {{(\ShopifyApp::shop()->plan->id == 1) ? 'active' : ''}}">
                            <div class="wow fade-down frames-second" data-wow-duration="2000ms" data-wow-delay="300ms">
                                <h1>BASIC</h1>
                                <div class="price-amount">
                                    <span class="price-tag">$10.99 </span>
                                    <span class="per_month">/Month</span>
                                </div>
                                <div class="price-memo">
                                    <p>3 Products</p>
                                    <p>Feature Two</p>
                                    <p>Feature Three</p>
                                    <p class="price-memo-inactive">Feature Four</p>
                                </div>
                                <a @if(\ShopifyApp::shop()->plan->id != 1) href="{{ route('billing', ['plan' => 1]) }}" @endif class="price-content-button plan-bttn">{{(\ShopifyApp::shop()->plan->id > 1) ? 'DOWNGRADE' : ((\ShopifyApp::shop()->plan->id < 1) ? 'UPGRADE' : 'CURRENT PLAN')}}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="price-content-second pro newc-price {{(\ShopifyApp::shop()->plan->id == 2) ? 'active' : ''}}">
                            <div class="wow fade-down frames1-second" data-wow-duration="2000ms" data-wow-delay="300ms">
                                <h1>STANDARD</h1>
                                <div class="price-amount">
                                    <span class="price-tag">$20.99 </span>
                                    <span class="per_month">/Month</span>
                                </div>
                                <div class="price-memo">
                                    <p>5 Products</p>
                                    <p>Feature Two</p>
                                    <p >Feature Three</p>
                                    <p class="price-memo-inactive">Feature Four</p>
                                </div>
                                <a @if(\ShopifyApp::shop()->plan->id != 2) href="{{ route('billing', ['plan' => 2]) }}" @endif class="price-content-button plan-bttn">{{(\ShopifyApp::shop()->plan->id > 2) ? 'DOWNGRADE' : ((\ShopifyApp::shop()->plan->id < 2) ? 'UPGRADE' : 'CURRENT PLAN')}}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="price-content-second pro {{(\ShopifyApp::shop()->plan->id == 3) ? 'active' : ''}}">
                            <div class="wow fade-down frames2-second" data-wow-duration="2000ms" data-wow-delay="300ms">
                                <h1>PREMIUM</h1>
                                <div class="price-amount ">
                                    <span class="price-tag">$30.99 </span>
                                    <span class="per_month">/Month</span>
                                </div>
                                <div class="price-memo">
                                    <p>10 Products</p>
                                    <p>Feature Two</p>
                                    <p >Feature Three</p>
                                    <p class="price-memo-inactive">Feature Four</p>
                                </div>
                                <a @if(\ShopifyApp::shop()->plan->id != 3) href="{{ route('billing', ['plan' => 3]) }}" @endif class="price-content-button plan-bttn">{{(\ShopifyApp::shop()->plan->id > 3) ? 'DOWNGRADE' : ((\ShopifyApp::shop()->plan->id < 3) ? 'UPGRADE' : 'CURRENT PLAN')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection
