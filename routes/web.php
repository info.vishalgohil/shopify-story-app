<?php

//Route::get('test', 'TestController@test');

Route::get('logout', function() {

    request()->session()->flush();
    return redirect()->route('home');
});

Route::group(['middleware' => ['auth.shop', 'billable']], function () {

    Route::view('/', 'app.dashboard.index')->name('home');
    Route::view('settings', 'app.settings.index')->name('settings');
    Route::view('pricing', 'app.pricing.index')->name('pricing');
});
