<?php

use Illuminate\Http\Request;

Route::group(['namespace' => 'API'], function () {

    Route::group(['middleware' => ['auth.shop:api']], function() {

        Route::get('/', 'ShopController@index');
        Route::post('/', 'ShopController@update');

        Route::get('dashboard', 'DashboardController@index');
        Route::get('widget', 'WidgetController@index');


        Route::group(['prefix' => 'products'], function () {

            Route::get('/', 'ProductController@index');
            Route::post('/', 'ProductController@store');
            Route::delete('{product}', 'ProductController@remove');
        });
    });

    Route::group(['namespace' => 'Shop', 'prefix' => 'shop'], function () {

        Route::get('/', 'ShopController@index');

        Route::group(['prefix' => '{shopifyShop}'], function () {

            Route::group(['prefix' => 'products'], function () {

                Route::get('/', 'ProductController@index');
                Route::post('{product}/view', 'ProductController@view');
                Route::post('{product}/visit', 'ProductController@visit');
            });

            Route::post('load', 'ShopController@load');
        });
    });
});
