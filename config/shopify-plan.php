<?php

return [
        'plans' =>[
            [
                'id' => 1,
                'type' => env('IS_RECURRING', 1),
                'name' => env('PLAN_NAME_1', "Basic"),
                'price' => env('PLAN_PRICE_1', 10.00),
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Get 15 Total Views"),
                'trial_days' => env('TRIAL_DAY', 14),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1)
            ],
            [
                'id' => 2,
                'type' => env('IS_RECURRING', 1),
                'name' => env('PLAN_NAME_2', "Premium"),
                'price' => env('PLAN_PRICE_2', 15.00),
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_2', "Get 25 Total Views"),
                'trial_days' => 0,
                'test' => 0,
                'on_install' => 0,
            ],
            [
                'id' => 3,
                'type' => env('IS_RECURRING', 1),
                'name' => env('PLAN_NAME_3', "Gold"),
                'price' => env('PLAN_PRICE_3', 20.00),
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_3', "Get Unlimited Total Views"),
                'trial_days' => 0,
                'test' => 0,
                'on_install' => 0,
            ],
        ],

        'total_view' =>[
            'plan_1' => env('TOTALVIEW_1', 15),
            'plan_2' => env('TOTALVIEW_2', 25),
            'plan_3' => env('TOTALVIEW_3', 0),
        ],

    ];
