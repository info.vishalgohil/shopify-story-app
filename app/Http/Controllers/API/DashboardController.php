<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function index(Request $request)
    {

        $shop = \ShopifyApp::shop();

        $totalViews = $shop->widget->total_loads + $shop->products()->withViewCount()->get()->sum('views');

        $totalClicks = $shop->insights()->sum('visits');

        $topProducts = $shop->products()->withVisitCount()->orderBy('visits', 'DESC')->take(5)->get();

        return response()->json(['total_views' => $totalViews, 'total_clicks' => $totalClicks, 'top_products' => $topProducts]);
    }
}
