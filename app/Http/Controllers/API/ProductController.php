<?php

namespace App\Http\Controllers\API;

use App\Eloquents\Product;
use App\Eloquents\ProductImage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index(Request $request)
    {

        $shop = \ShopifyApp::shop();

        $products = $shop->products;

        return response()->json(['products' => $products]);
    }

    public function store(Request $request)
    {

        $shop = \ShopifyApp::shop();

        $currentProductsCount = $shop->products()->count();

        if ($currentProductsCount + count($request->ids) > 10)
        {
            return response()->json(['error' => 'You can add only ' . (10 - $currentProductsCount) . ' products.'], 422);
        }

        $shopifyProducts = $shop->api()->graph($this->productsQuery($request->ids))->body->nodes;
        foreach ($shopifyProducts as $shopifyProduct)
        {

            $product = Product::withTrashed()->firstOrNew(['id' => $shopifyProduct->legacyResourceId]);
            $product->shop_id = $shop->id;
            $product->title = $shopifyProduct->title;
            $product->handle = $shopifyProduct->handle;
            $product->image = ($shopifyProduct->featuredImage) ? $shopifyProduct->featuredImage->src : null;
            $product->price = $shopifyProduct->priceRange->minVariantPrice->amount;
            if ($product->trashed()) {
                $product->restore();
            } else {
                $product->save();
            }

            $productImageIDs = [];
            foreach ($shopifyProduct->images->edges as $image)
            {

                $imageID = str_replace('gid://shopify/ProductImage/', '', $image->node->id);
                $productImage = ProductImage::firstOrNew(['id' => $imageID]);
                $productImage->product_id = $shopifyProduct->legacyResourceId;
                $productImage->src = $image->node->src;
                $productImage->master = ($image->node->id == $shopifyProduct->featuredImage->id);
                $productImage->save();

                $productImageIDs[] = $imageID;
            }
            if (!empty($productImageIDs)) {
                $product->images()->whereNotIn('id', $productImageIDs)->delete();
            }
        }

        return response()->json(['message' => 'Product(s) added!']);
    }

    public function remove(Request $request, Product $product)
    {

        $product->delete();
        return response()->json(['message' => 'Product deleted!']);
    }

    protected function productsQuery($productIDs)
    {
        return '{nodes(ids: ' . json_encode($productIDs) . ') {... on Product {legacyResourceId handle title featuredImage {src id originalSrc} images(first: 10) {edges {node {id src originalSrc}}} priceRange {minVariantPrice {amount}}onlineStoreUrl}}}';
    }
}
