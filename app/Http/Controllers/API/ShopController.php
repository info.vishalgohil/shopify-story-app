<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ShopController extends Controller
{

    public function index(Request $request)
    {

        $shop = \ShopifyApp::shop();
        return response()->json(['shop' => $shop]);
    }

    public function update(Request $request)
    {

        $shop = \ShopifyApp::shop();

        $shop->enabled = (isset($request->enabled) && is_bool($request->enabled)) ? $request->enabled : $shop->enabled;
        $shop->save();

        return response()->json(['message' => 'Saved']);
    }
}
