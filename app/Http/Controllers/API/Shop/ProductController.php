<?php

namespace App\Http\Controllers\API\Shop;

use App\Eloquents\Product;
use App\Eloquents\Shop;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index(Request $request, Shop $shopifyShop)
    {

        $products = $shopifyShop->products()->with('images')->get();

        return response()->json(['products' => $products]);
    }

    public function view(Request $request, Shop $shopifyShop, Product $product)
    {

        $date = Carbon::today();

        $insight = $product->insights()->firstOrNew(['date' => $date]);
        $insight->views = $insight->views + 1;
        $insight->save();

        return response()->json(['message' => 'Updated']);
    }

    public function visit(Request $request, Shop $shopifyShop, Product $product)
    {

        $date = Carbon::today();

        $insight = $product->insights()->firstOrNew(['date' => $date]);
        $insight->visits = $insight->visits + 1;
        $insight->save();

        return response()->json(['message' => 'Updated']);
    }
}
