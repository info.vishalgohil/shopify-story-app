<?php

namespace App\Http\Controllers\API\Shop;

use App\Eloquents\Shop;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ShopController extends Controller
{

    public function index(Request $request)
    {

        $shop = Shop::where('shopify_domain', $request->shopify_domain)->first();
        return response()->json(['shop' => $shop]);
    }

    public function load(Request $request, Shop $shopifyShop)
    {

        $widget = $shopifyShop->widget;
        $widget->total_loads = (int)$widget->total_loads + 1;
        $widget->save();

        return response()->json(['message' => 'Updated']);
    }
}
