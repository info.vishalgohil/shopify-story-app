<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WidgetController extends Controller
{
    public function index(Request $request)
    {

        $shop = \ShopifyApp::shop();
        $widget = $shop->widget;
        return response()->json($widget);
    }
}
