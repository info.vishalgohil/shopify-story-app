<?php

namespace App\Eloquents;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function insights()
    {
        return $this->hasMany(Insight::class);
    }

    public function scopeWithViewCount($query)
    {
        return $query->select(\DB::raw('products.*, sum(views) as views'))->join('insights', 'products.id', '=', 'insights.product_id')->groupBy('products.id');
    }

    public function scopeWithVisitCount($query)
    {
        return $query->select(\DB::raw('products.*, sum(visits) as visits'))->join('insights', 'products.id', '=', 'insights.product_id')->groupBy('products.id');
    }
}
