<?php

namespace App\Eloquents;

use OhMyBrew\ShopifyApp\Models\Shop as OhMyBrewShop;

class Shop extends OhMyBrewShop
{

    protected $guarded = [];
    protected $casts = ['grandfathered' => 'boolean', 'freemium' => 'boolean'];

    public function widget()
    {
        return $this->hasOne(Widget::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function insights()
    {
        return $this->hasManyThrough(Insight::class, Product::class);
    }
}
