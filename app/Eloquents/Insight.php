<?php

namespace App\Eloquents;

use Illuminate\Database\Eloquent\Model;

class Insight extends Model
{

    public $timestamps = false;
    protected $guarded = [];
    protected $casts = ['date' => 'date:Y-m-d', 'views' => 'integer', 'visits' => 'integer'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
