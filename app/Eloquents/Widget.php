<?php

namespace App\Eloquents;

use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    protected $guarded = [];
    protected $casts = ['status' => 'boolean', 'total_loads' => 'integer'];

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
}
