<?php

if (!function_exists('product_price')) {
    function product_price($variants) {

        $variants = !is_array($variants) ? (array)$variants : $variants;
        $price = null;
        foreach ($variants as $variant)
        {
            if (!$price || (int)$variant['price'] < $price)
            {
                $price = $variant['price'];
            }
        }

        return $price;
    }
}

if (!function_exists('product_price_with_compare')) {
    function product_price_with_compare($variants) {

        $variants = !is_array($variants) ? (array)$variants : $variants;
        $price = null;
        $comparePrice = null;
        foreach ($variants as $variant)
        {
            if (!$price || (int)$variant['price'] < $price)
            {
                $price = $variant['price'];
                $comparePrice = $variant['compare_at_price'];
            }
        }

        return [$price, $comparePrice];
    }
}
