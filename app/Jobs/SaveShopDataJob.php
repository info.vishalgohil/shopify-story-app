<?php

namespace App\Jobs;

use App\Eloquents\Shop;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SaveShopDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $shop;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Shop $shop)
    {
        $this->shop = $shop;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if (!$this->shop->shopify_id || !$this->shop->money_format) {

            $api = $this->shop->api();
            $storeRequest = $api->rest('GET', '/admin/shop.json');
            $this->shop->shopify_id = $storeRequest->body->shop->id;
            $this->shop->money_format = $storeRequest->body->shop->money_format;
            $this->shop->save();
        }
    }
}
