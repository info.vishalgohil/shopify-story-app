<?php

namespace App\Jobs;

use App\Eloquents\Shop;
use App\Eloquents\Widget;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateWidgetJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $shop;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Shop $shop)
    {
        $this->shop = $shop;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if (!$this->shop->widget) {

            $api = $this->shop->api();
            $storeRequest = $api->rest('GET', '/admin/shop.json');

            $widget = Widget::firstOrNew(['shop_id' => $this->shop->id]);
            $widget->money_format = $storeRequest->body->shop->money_format;
            $widget->save();
        }
    }
}
