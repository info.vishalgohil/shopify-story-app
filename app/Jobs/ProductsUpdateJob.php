<?php

namespace App\Jobs;

use App\Eloquents\Shop;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProductsUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $shopDomain;
    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $shop = Shop::select('id')->where('shopify_domain', $this->shopDomain)->first();

        $product = $shop->products()->withTrashed()->find($this->data->id);
        $product->title = $this->data->title;
        $product->handle = $this->data->handle;
        $product->image = ($this->data->image) ? $this->data->image->src : null;
        $product->price = null;

        foreach ($this->data->variants as $variant)
        {

            if (is_null($product->price) || $variant->price < $product->price) {

                $product->price = $variant->price;
                $product->compare_at_price = $variant->compare_at_price;
            }
        }
        $product->save();

        $productsImgeIDs = array_column((array) $this->data->images, 'id');
        $product->images->whereNotIn('id', $productsImgeIDs)->delete();
        foreach ($this->data->images as $image) {

            $productImage = $product->images()->firstOrNew(['id' => $image->id]);
            $productImage->src = $image->src;
            $productImage->save();
        }
    }
}
