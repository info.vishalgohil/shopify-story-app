<?php

use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = config('shopify-plan.plans');
        foreach ($plans as $key => $val){
            \DB::table('plans')->updateOrInsert(
                [ 'id' => $val['id'] ],
                 $val
            );
        }
    }
}
