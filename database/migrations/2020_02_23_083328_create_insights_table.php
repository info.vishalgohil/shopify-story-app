<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insights', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_id');
            $table->date('date');
            $table->bigInteger('views')->default(0);
            $table->bigInteger('visits')->default(0);

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('insights', function (Blueprint $table) {
            // @codeCoverageIgnoreStart
            if (DB::getDriverName() != 'sqlite') {
                $table->dropForeign(['product_id']);
            }
            // @codeCoverageIgnoreEnd
        });

        Schema::dropIfExists('insights');
    }
}
