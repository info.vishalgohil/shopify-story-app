<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->string('id')->primary();
            $table->unsignedBigInteger('shop_id');
            $table->string('handle', 1000);
            $table->string('title');
            $table->string('image', 1000)->nullable()->default(null);
            $table->decimal('price', 10, 2);
            $table->decimal('compare_at_price', 10, 2)->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('products', function (Blueprint $table) {
            // @codeCoverageIgnoreStart
            if (DB::getDriverName() != 'sqlite') {
                $table->dropForeign(['shop_id']);
                $table->dropForeign(['image_id']);
            }
            // @codeCoverageIgnoreEnd
        });

        Schema::dropIfExists('products');
    }
}
