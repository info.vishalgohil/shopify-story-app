<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('namespace')->nullable(true)->default(null);
            $table->string('shopify_id')->nullable(true)->default(null);
            $table->string('shopify_domain');
            $table->string('shopify_token')->nullable(true)->default(null);
            $table->boolean('grandfathered')->default(false);
            $table->boolean('freemium')->default(false);
            $table->string('money_format')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shops');
    }
}
