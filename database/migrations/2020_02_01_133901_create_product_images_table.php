<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_images', function (Blueprint $table) {

            $table->string('id')->primary();
            $table->string('product_id');
            $table->string('src', 1000);
            $table->tinyInteger('position')->nullable()->default(null);
            $table->boolean('master')->default(false);
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('product_images', function (Blueprint $table) {
            // @codeCoverageIgnoreStart
            if (DB::getDriverName() != 'sqlite') {
                $table->dropForeign(['product_id']);
            }
            // @codeCoverageIgnoreEnd
        });

        Schema::dropIfExists('product_images');
    }
}
