<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWidgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widgets', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->unsignedBigInteger('shop_id');
            $table->boolean('status')->default(true);
            $table->string('total_loads')->default(0);
           // $table->string('title')->nullable()->default(null);
           // $table->string('story_style')->default('slider');
           // $table->json('style')->nullable()->default(null);
            $table->string('money_format');
            $table->timestamps();

            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('widgets', function (Blueprint $table) {
            // @codeCoverageIgnoreStart
            if (DB::getDriverName() != 'sqlite') {
                $table->dropForeign(['shop_id']);
            }
            // @codeCoverageIgnoreEnd
        });

        Schema::dropIfExists('widgets');
    }
}
