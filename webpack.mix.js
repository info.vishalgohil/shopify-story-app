const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/product-gallery.js', 'public/js')
    .sass('resources/scss/app.scss', 'public/css')
    .sass('resources/scss/product-gallery.scss', 'public/css')
    .copyDirectory('resources/css', 'public/css')
    .copyDirectory('resources/images', 'public/images')
    .copyDirectory('resources/fonts', 'public/fonts');


if (mix.inProduction()) {
    mix.version();
}
